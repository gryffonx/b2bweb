﻿using System.Web;
using System.Web.Mvc;
using B2BDotCom.Configuration;

namespace B2BDotCom
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new RequireHttpsAttribute());
            filters.Add(new RequireHstsAttribute(31536000) { IncludeSubDomains = true, Preload = true });
        }
    }
}
