﻿@model dynamic

@section navigation {
    @Html.Partial("_ServicesMenu")
}


<p>
    Broker to Broker prepares the Automated Customer Account Transfer (ACAT) forms and other necessary documents for clients on your behalf when you are moving to a new firm. Mailings can be sent directly to your clients or to the coordinating office for distribution.
</p>
<p> Your needs are the foundation of our services, and those needs can vary widely. Each project will be customized to satisfy your individual requests and make the best possible presentation to your clients.</p>
<p><strong>Below is an overview of the services we provide in the process of creating and fulfilling your mailing projects:</strong></p>
<p>
    <span class="style3mini"><strong>Project Management</strong></span><strong>
        <br>
    </strong>Broker to Broker works closely with you, your branch, or your transition team to coordinate all of the necessary components of the transfer mailing prior to the date the advisor joins the team. Strict adherence to compliance and privacy regulations helps ensure a smooth transition.
</p>
<p>
    <span class="style3mini"><strong>Data Entry</strong></span><strong>
        <br>
    </strong>Through our secure, proprietary, privacy and protocol compliant processes, your data can be uploaded into our systems and merged with your forms quickly and efficiently. Our entire system is designed to expedite the delivery of your packages to your clients.
</p>
<p>
    <span class="style3mini"><strong>Forms Population</strong></span><strong>
        <br>
    </strong>Through an extensive library of compliance approved forms, forms provided by you, and close communication throughout the pre-mailing process, Broker to Broker can ensure accurate and professional population of the data on your forms for delivery to your clients.
</p>
<p>
    <span class="style3mini"><strong>Printing</strong></span><strong>
        <br>
    </strong>Whether it’s black and white, color, one-sided, two-sided, stapled, forms, letterhead, or announcements, Broker to Broker can print your transfer documents to get them out the door quickly, accurately, and with a professional presentation.
</p>
<p>
    <span class="style3mini"><strong>Collation and Householding</strong></span><strong>
        <br>
    </strong>Although the process is highly automated, Broker to Broker often receives requests that cannot be accomplished through technology. Items such as “Sign Here” stickers and paperclips have to be manually affixed. This slows the process down somewhat, but manual requests are no problem for our Broker to Broker collation team. On top of that, accounts are consolidated into household mailings, which significantly reduces your overall costs and improves your clients’ transfer experience.
</p>
<p>
    <span class="style3mini"><strong>Return Envelopes</strong></span><strong>
        <br>
    </strong>Return envelopes can be included in your mailings in a variety of ways and are highly suggested to make the transfer process as easy as possible for your clients. Whether it is United States Postal Service (USPS) Business Reply envelopes that you supply, or either of the two major overnight carriers, Broker to Broker can make sure that your clients have a simple and convenient channel to return the transfer documents to you.
</p>
<p>
    <span class="style3mini"><strong>Postage</strong></span><strong>
        <br>
    </strong>Your transfer packets can be prepared in a variety of ways. Broker to Broker can deliver your mailings directly to your clients through the USPS or either of the two major overnight carriers, or return the completed mailings in a box to your office for you to deliver at your convenience.
</p>
<p>
    <span class="style3mini"><strong>Wrap Up Tools</strong></span><strong>
        <br>
    </strong>When your project is complete, Broker to Broker can deliver the following items:
</p>
<p>&gt; Follow-Up Report: This document-tracking tool includes titles, addresses, and telephone numbers to assist you in tracking the documents as they are returned from your clients.</p>
<p>&gt; Data CD: To assist with subsequent mailings, the CD includes the announcement letter, scanned signature, and the names and addresses used for the original mailing.</p>
<p>&gt; File Folder Labels: These labels can be pre-populated with the account title and contain a pre-formatted space for you to add the new account number as the accounts are opened.</p>
