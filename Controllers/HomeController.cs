﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using B2BDotCom.Models;
using Stripe;
using Stripe.Checkout;

namespace B2BDotCom.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home";
            return View();
        }

        public ActionResult Faq()
        {
            ViewBag.Title = "Frequently Asked Questions";
            return View();
        }

        public ActionResult Sitemap()
        {
            ViewBag.Title = "Site Map";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Title = "Contact Us";
            return View();
        }

        public ActionResult SubmitPayment()
        {
            ViewBag.Title = "Submit Payment";
            return View();
        }
        public ActionResult PaymentSuccess()
        {
            ViewBag.Title = "Payment Success";
            return View();
        }
        public ActionResult PaymentError()
        {
            ViewBag.Title = "Payment Error";
            return View();
        }

        /// <summary>
        /// Create Payment intents.  This will be invoked via a javascript fetch object.  Will need to pass some data to this (amount, etc)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Secret()
        {
            var resolveRequest = HttpContext.Request;
            Payment pmt = null;
            resolveRequest.InputStream.Seek(0, SeekOrigin.Begin);
            string jsonString = new StreamReader(resolveRequest.InputStream).ReadToEnd();
            if (jsonString != null)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                pmt = (Payment) serializer.Deserialize(jsonString, typeof(Payment));
            }

            var ll = Convert.ToInt64(compute_cents(pmt.amount));


            var uniqueId = (DateTime.Now.Ticks - new DateTime(2016, 1, 1).Ticks).ToString("x").ToUpper();
            var options = new PaymentIntentCreateOptions
            {
                Amount = ll,
                Currency = "usd",
                Description = "Invoice #" + pmt.invoiceNbr,
                ReceiptEmail = pmt.email,
                // Verify your integration in this guide by including this parameter
                Metadata = new Dictionary<string, string>
                {
                    { "integration_check", "accept_a_payment" },
                    { "receipt_email", pmt.email },
                    { "invoice_number", pmt.invoiceNbr },
                    { "transaction_number", uniqueId },
                },
            };

            var service = new PaymentIntentService();
            var paymentIntent = service.Create(options);
            return Json(paymentIntent, JsonRequestBehavior.AllowGet);
        }

        static int compute_cents(double dollar_amount)
        {
            return (int)(dollar_amount * 100);
        }
    }
}
