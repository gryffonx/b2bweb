﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace B2BDotCom.Controllers
{
    public class ProcessController : Controller
    {
        // GET: Process
        public ActionResult Index()
        {
            ViewBag.Title = "Process in a Nutshell";
            return View();
        }

        public ActionResult HowToPrepare()
        {
            ViewBag.Title = "How the Broker Prepares";
            return View();
        }

        public ActionResult HowBranchPrepares()
        {
            ViewBag.Title = "How the Branch Prepares";
            return View();
        }
    }
}