﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace B2BDotCom.Controllers
{
    public class ServicesController : Controller
    {
        // GET: Services
        public ActionResult Index()
        {
            ViewBag.Title = "Basic Projects";
            return View();
        }

        public ActionResult Overview()
        {
            ViewBag.Title = "Overview";
            return View();
        }

        public ActionResult AnnouncementServices()
        {
            ViewBag.Title = "Announcement Services";
            return View();
        }

        public ActionResult SpecialProjects()
        {
            ViewBag.Title = "Special Projects";
            return View();
        }

        public ActionResult Pricing()
        {
            ViewBag.Title = "Pricing";
            return View();
        }
    }
}