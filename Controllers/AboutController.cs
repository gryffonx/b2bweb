﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace B2BDotCom.Controllers
{
    public class AboutController : Controller
    {
        // GET: About
        public ActionResult Index()
        {
            ViewBag.Title = "About Us";
            return View();
        }

        public ActionResult History()
        {
            ViewBag.Title = "History";
            return View();
        }

        public ActionResult Management()
        {
            ViewBag.Title = "Management";
            return View();
        }

        public ActionResult Testimonials()
        {
            ViewBag.Title = "Testimonials";
            return View();
        }

        public ActionResult Compliance()
        {
            ViewBag.Title = "Compliance Information Security";
            return View();
        }
    }
}