﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace B2BDotCom.Controllers
{
    public class TurnAroundController : Controller
    {
        // GET: TurnAround
        public ActionResult Index()
        {
            ViewBag.Title = "Basic Projects";
            return View();
        }

        public ActionResult RapidTurnaround()
        {
            ViewBag.Title = "Rapid Turnaround";
            return View();
        }
    }
}