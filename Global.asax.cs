﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Stripe;

namespace B2BDotCom
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNet-Version");
            Response.Headers.Remove("X-AspNetMvc-Version");

            Response.AddHeader("X-XSS-Protection", "1");
            Response.AddHeader("X-Content-Type-Options", "nosniff");
            if (Response.Headers.Get("X-Frame-Options") == null)
            {
                Response.AddHeader("X-Frame-Options", "SAMEORIGIN");
            }
        }


        protected void Application_Start()
        {
            
            StripeConfiguration.ApiKey = ConfigurationManager.AppSettings["StripeKey"];
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            RegisterBundles(BundleTable.Bundles);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            MvcHandler.DisableMvcResponseHeader = true;


            //var options = new WebhookEndpointCreateOptions
            //{
            //    Url = "https://localhost/webhook/endpoint",
            //    EnabledEvents = new List<string>
            //    {
            //        "charge.failed",
            //        "charge.succeeded",
            //    },
            //};
            //var service = new WebhookEndpointService();
            //service.Create(options);
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            var bundle = new ScriptBundle("~/bundles/allscripts").Include(
                "~/Scripts/kendo/2021.1.119/jquery.min.js",
                "~/Scripts/kendo/2021.1.119/angular.min.js",
                "~/Scripts/kendo/2021.1.119/jszip.min.js",
                "~/Scripts/kendo/2021.1.119/kendo.all.min.js",
                "~/Scripts/kendo/2021.1.119/kendo.aspnetmvc.min.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/kendo.modernizr.custom.js");

            bundle.Orderer = new NonOrderingBundleOrderer();

            bundles.Add(bundle);


            var styleBundle = new StyleBundle("~/Content/allstyles")
                .Include("~/Content/kendo/2021.1.119/kendo.bootstrap-v4.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Content/Site.css", new CssRewriteUrlTransform());

            styleBundle.Orderer = new NonOrderingBundleOrderer();

            bundles.Add(styleBundle);


            BundleTable.EnableOptimizations = true;
        }

    }
    internal class NonOrderingBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }

        public IEnumerable<FileInfo> OrderFiles(BundleContext context, IEnumerable<FileInfo> files)
        {
            return files;
        }
    }

}
