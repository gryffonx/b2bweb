﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace B2BDotCom.Models
{
    public class Payment
    {
        public string email { get; set; }
        public double amount { get; set; }
        public string invoiceNbr { get; set; }
    }
}