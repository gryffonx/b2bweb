﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace B2BDotCom.Configuration
{
    /// <summary>
    /// Represents an attribute that forces an unsecured HTTP request to be re-sent over HTTPS and adds HSTS headers to secured requests.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class RequireHstsAttribute : RequireHttpsAttribute
    {

        private readonly uint _maxAge;
        private const string StrictTransportSecurityHeader = "Strict-Transport-Security";
        private const string MaxAgeDirectiveFormat = "max-age={0}";
        private const string IncludeSubDomainsDirective = "; includeSubDomains";
        private const string PreloadDirective = "; preload";
        private const int MinimumPreloadMaxAge = 10886400;



        /// <summary>
        /// Gets the time (in seconds) that the browser should remember that this resource is only to be accessed using HTTPS.
        /// </summary>
        public uint MaxAge
        {
            get
            {
                return _maxAge;
            }
        }

        /// <summary>
        /// Gets or sets the value indicating if this rule applies to all subdomains as well.
        /// </summary>
        public bool IncludeSubDomains { get; set; }

        /// <summary>
        /// Gets or sets the value indicating if subscription to HSTS preload list (https://hstspreload.appspot.com/) should be confirmed.
        /// </summary>
        public bool Preload { get; set; }

        /// <summary>
        /// Initializes a new instance of the RequireHstsAttribute class.
        /// </summary>
        /// <param name="maxAge">The time (in seconds) that the browser should remember that this resource is only to be accessed using HTTPS.</param>
        public RequireHstsAttribute(uint maxAge) : base()
        {
            _maxAge = maxAge;
            IncludeSubDomains = false;
            Preload = false;
        }

        /// <summary>
        /// Determines whether a request is secured (HTTPS). If it is sets the Strict-Transport-Security header. If it is not calls the HandleNonHttpsRequest method.
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (filterContext.HttpContext.Request.IsSecureConnection)
            {
                if (Preload && (MaxAge < MinimumPreloadMaxAge))
                {
                    throw new InvalidOperationException("In order to confirm HSTS preload list subscription expiry must be at least eighteen weeks (10886400 seconds).");
                }

                if (Preload && !IncludeSubDomains)
                {
                    throw new InvalidOperationException("In order to confirm HSTS preload list subscription subdomains must be included.");
                }

                var headerBuilder = new StringBuilder();
                headerBuilder.AppendFormat(MaxAgeDirectiveFormat, _maxAge);

                if (IncludeSubDomains)
                {
                    headerBuilder.Append(IncludeSubDomainsDirective);
                }

                if (Preload)
                {
                    headerBuilder.Append(PreloadDirective);
                }
                filterContext.HttpContext.Response.Headers.Remove("server");
                filterContext.HttpContext.Response.Headers.Remove("x-aspnet-version");
                filterContext.HttpContext.Response.Headers.Remove("x-aspnetmvc-version");
                filterContext.HttpContext.Response.Headers.Remove("x-powered-by");

                filterContext.HttpContext.Response.AppendHeader(StrictTransportSecurityHeader, headerBuilder.ToString());
            }
            else
            {
                HandleNonHttpsRequest(filterContext);
            }
        }
    }
}